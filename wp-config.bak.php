<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'testassignment' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'test123' );

/** MySQL hostname */
define( 'DB_HOST', '127.0.0.1' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',          'dpkmQgyy]0fSra/~RN3jKKV.YOi4H:MQd1: Oj,tx}]nSlm6.k LTEL~>@WK&xO/' );
define( 'SECURE_AUTH_KEY',   'h3$Jbai!cKe0rSWc*tMD&4tZ#XUC49^~j.F=)Yr!YA7(SIbF!*Lzqsw0>!QL(:9_' );
define( 'LOGGED_IN_KEY',     'AYV%>p uxaLQe82J,i(v<CF% gFM7ge!5+DaD2-1c8Lhl+Va-&]{vZF,&G`w;<rG' );
define( 'NONCE_KEY',         '>#Z&&GE:^{87@0+$wBeNrE P=UV&RD`R}JtzyWD&QpY.Nro!Of,So4)D7/Wry v>' );
define( 'AUTH_SALT',         '3++0E}Ka8:A6u_rz[zua}9d<~u=}RYnH`C@RVzG~ d(_]fQ-;nkc Y7{t6=7V=iU' );
define( 'SECURE_AUTH_SALT',  '3B?HkWdT-{:V>BlDC^i6ZFLPxBO5sJpdjlE_Z]L]N0G3?jenUfgXYENQNJ6yp&.P' );
define( 'LOGGED_IN_SALT',    'FqZhF;JC1e2NylVTbo8.WtEuxjnQRPI[PzIOC6@oJTfC@{Ln]2`J[5|?EZ-NdM7Q' );
define( 'NONCE_SALT',        'Z9R%AR.<w+r*ypE]=}%Yq1>)j+gQ_CPki0g9;X~E;No^4|Ll/Ie?Ahb^!O)7{7y2' );
define( 'WP_CACHE_KEY_SALT', '8Klvd#A_e X8;~?0L<s=Uq(^FxYUiU ^eKqz=c<e;pLwCK&nYS1p|=+$Hi|1}e~G' );

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';


define('WP_DEBUG', true);

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
