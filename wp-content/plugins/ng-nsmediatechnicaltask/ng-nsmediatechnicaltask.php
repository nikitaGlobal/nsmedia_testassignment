<?php
    
    /**
     * Plugin Name: NG Test task
     * Plugin URI: https://nikita.global
     * Description: Test task for NS Media
     * Author: Nikita Menshutin
     * Version: 1.0
     * Text Domain: ngnsmediatechnicaltask
     * Domain Path: languages
     *
     * PHP version 7.2
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <nikita@nikita.global>
     * @license  https://nikita.global commercial
     * @link     https://nikita.global
     * */
    defined('ABSPATH') or die("No script kiddies please!");
    
    /**
     * Our main class goes here
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <wpplugins@nikita.global>
     * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
     * @link     https://nikita.global
     */
Class Ngnsmediatechnicaltask
{
        
    /**
         * Construct
         *
         * @return void
         **/
    public function __construct()
    {
        $this->prefix        = self::prefix();
        $this->version       = self::version();
        $this->pluginName    = __('NG Test task');
        $this->visitorsTable = self::tableName();
        load_plugin_textdomain(
            $this->prefix,
            false,
            $this->prefix . '/languages'
        );
        add_action(
            'customize_register',
            array($this, 'customizeRegister'),
            1000
        );
        $this->themeOptions = array(
            array(
                'key'   => 'footerCode', //do not rename!
                'label' => __('place your js snippet here (GA for example)')
            )
        );
        add_action('wp_footer', array($this, 'wpFooter'), 9999);
        add_action(
            'template_redirect', array($this, 'saveVisitorData'),
            9999
        );
    }
        
    /**
         * Registering theme options
         *
         * @param object $wp_customize customizer object
         *
         * @return void
         */
    public function customizeRegister($wp_customize)
    {
        $wp_customize->add_section(
            $this->prefix, array(
                'title'       => $this->pluginName,
                'description' => '',
                'priority'    => 120,
            )
        );
        foreach ($this->themeOptions as $set) {
            $wp_customize->add_setting(
                $this->prefix . $set['key'],
                array(
                    'default'    => '',
                    'id'         => $this->prefix . $set['key'],
                    'capability' => 'edit_theme_options',
                    'type'       => 'option',
                )
            );
            $wp_customize->add_control(
                $this->prefix . $set['key'], array(
                    'label'    => $set['label'],
                    'section'  => $this->prefix,
                    'settings' => $this->prefix . $set['key'],
                    'type'     => 'textarea'
                )
            );
        }
    }
        
    /**
         * Printing custom code in footer
         *
         * @return void
         */
    public function wpFooter()
    {
        $footerCode = get_option($this->prefix . 'footerCode');
        if (! $footerCode) {
            return;
        }
        echo $footerCode;
    }
        
    /**
         * Saving visitors data to table row
         *
         * @return false|int
         */
    public function saveVisitorData()
    {
        global $wpdb;
        $table = $wpdb->prefix . self::tablename();
            
        $row = array(
            'ip'      => (string)$_SERVER['REMOTE_ADDR'],
            'url'     => (string)$this->_getCurrentUrl(),
            'browser' => (string)$_SERVER['HTTP_USER_AGENT'],
            'userid'  => (int)get_current_user_id(),
            'rawdata' => (string)serialize($_SERVER)
        );
            
        return $wpdb->insert($table, $row);
    }
        
    /**
         * Getting current URL including get queries
         *
         * @return string url
         */
    private function _getCurrentUrl()
    {
        return $_SERVER['REQUEST_SCHEME'] .
               '//' .
               $_SERVER['SERVER_NAME'] .
               $_SERVER['REQUEST_URI'];
    }
        
    /**
         * Creating table if not exists during plugin activation
         *  Table structure
         * IP, Visited URL, browser data, rawdata, userid, timestamp
         *
         * @return void
         */
    public static function pluginActivation()
    {
        global $wpdb;
        $tablename = $wpdb->prefix . self::tableName();
        if ($wpdb->get_var(
            "SHOW TABLES LIKE '" . $tablename . "'"
        ) == $tablename
        ) {
            return;
        }
        /**
             * Table structure
             * IP, Visited URL, browser data, rawdata, userid, timestamp
             */
        $sql = 'CREATE TABLE ' . $tablename;
        $sql .= '(ip varchar(100) NOT NULL,' .
                'url text  NOT NULL,' .
                'browser text NOT NULL,' .
                'userid int(11) ,' .
                'rawdata text NOT NULL,' .
                'visited timestamp NOT NULL DEFAULT current_timestamp()' .
                ') ' . $wpdb->get_charset_collate() . ';';
            
        if (! function_exists('dbDelta')) {
            include_once ABSPATH . 'wp-admin/includes/upgrade.php';
        }
            
        dbDelta($sql);
            
    }
        
    /**
         * Method returns table name
         * for logging visitors
         *
         * @return string
         */
    public static function tableName()
    {
        return 'ng_visitors';
    }
        
    /**
         * Method returns prefix
         *
         * @return string prefix
         **/
    public static function prefix()
    {
        return 'ngnsmediatechnicaltask';
    }
        
    /**
         * Method returns plugin version
         *
         * @return string version
         **/
    public static function version()
    {
        return '1.0';
    }
}
    
    new Ngnsmediatechnicaltask();
    register_activation_hook(
        __FILE__,
        array('Ngnsmediatechnicaltask', 'pluginActivation')
    );
