=== NG NS Media technical task ===
Contributors: nikitaglobal
Plugin Name: NG Test task
Tags: work, aprobation, fortune, itlife
Author: Nikita Menshutin
Text Domain: Ngnsmediatechnicaltask
Domain Path: /languages
Requires at least: 3.6
Tested up to: 5.4.1
Stable tag: 1.0
Requires PHP: 5.6
Version: 1.0
License: 			GPLv2 or later
License URI: 		http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Saves visitors data to table, outputs user defined custom code to footer

[https://nikita.global/](https://nikita.global)


= Filters =
No
= Actions =
No

== Installation ==

Use WordPress' Add New Plugin feature, searching "NG Test task or download the archive and:

1. Unzip the archive on your computer  
2. Upload plugin directory to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress
4. Paste your custom code to Appearance -> Customize -> NG Test task

== Changelog ==

= 1.0 (2020-05-29) =
* Done, PSR-2 compliant
