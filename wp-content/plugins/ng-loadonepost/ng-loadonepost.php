<?php
    
    /**
     * Plugin Name: NG Load one post
     * Plugin URI: https://nikita.global
     * Description: Implements AJAX loading (Infinite Scroll) for the article pages.
     * Author: Nikita Menshutin
     * Version: 1.0
     * Text Domain: NgLoadonepost
     * Domain Path: languages
     *
     * PHP version 7.2
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <nikita@nikita.global>
     * @license  https://nikita.global commercial
     * @link     https://nikita.global
     * */
    defined('ABSPATH') or die("No script kiddies please!");
    
    /**
     * Our main class goes here
     *
     * @category NikitaGlobal
     * @package  NikitaGlobal
     * @author   Nikita Menshutin <wpplugins@nikita.global>
     * @license  http://opensource.org/licenses/gpl-license.php GNU Public License
     * @link     https://nikita.global
     */
Class NgLoadonepost
{
        
    /**
         * Construct
         *
         * @return void
         **/
    public function __construct()
    {
        $this->prefix     = self::prefix();
        $this->version    = self::version();
        $this->pluginName = __('NG Load one post');
            
        load_plugin_textdomain(
            $this->prefix,
            false,
            $this->prefix . '/languages'
        );
        add_action('wp_enqueue_scripts', array($this, 'scripts'));
        add_filter('the_content', array($this, 'filterTheContent'));
        add_action(
            'wp_ajax_nopriv_'.$this->prefix,
            array($this, 'ajaxLoadNextPost')
        );
        add_action(
            'wp_ajax_'.$this->prefix,
            array($this, 'ajaxLoadNextPost')
        );
    }
        
    /**
         * Ajax event for loading next post
         * and other data
         *
         * @return void
         */
    public function ajaxLoadNextPost()
    {
        if (! isset($_REQUEST['nextPostID'])) {
            wp_die();
        }
        $currentPost = (int)$_REQUEST['nextPostID'];
        global $post;
        $post     = get_post($currentPost);
        $id       = get_the_id();
        $nextPost = $this->_getNextPostId();
        $content  = $post->post_content;
        $content  = apply_filters('the_content', $content);
        $content  = apply_filters($this->prefix, $content);
        $content  = str_replace(']]>', ']]&gt;', $content);
        wp_send_json(
            array(
                'nextPostId'  => $nextPost,
                'content'     => $content,
                'title'       => $post->post_title,
                'nextPostUrl' => preg_replace(
                    '/http[^\/]+\/\/[^\/]*/',
                    '',
                    get_permalink($post)
                )
            )
        );
    }
        
    /**
         * Enqueue scripts
         *
         * @return void
         **/
    public function scripts()
    {
        if (! is_single()) {
            return;
        }
        wp_register_script(
            $this->prefix,
            plugin_dir_url(__FILE__) . '/scroll.js',
            array('jquery'),
            $this->version,
            true
        );
        wp_localize_script(
            $this->prefix,
            $this->prefix,
            array(
                'ajax_url'   => admin_url('admin-ajax.php'),
                'nextPostId' => $this->_getNextPostId()
            )
        );
        wp_enqueue_script($this->prefix);
    }
        
    /**
         * Add "marker" tag for firing load more event
         *
         * @param string $content post content
         *
         * @return string
         */
    public function filterTheContent($content)
    {
        if (wp_doing_ajax() || ! is_single()) {
            return $content;
        }
            
        return $content . '<div class="ngloadnext"></div>';
    }
        
    /**
         * Get next post id
         *
         * @return bool|int
         */
    private function _getNextPostId()
    {
        $nextpost = get_next_post();
        if (! is_object($nextpost)) {
            return false;
        }
            
        return $nextpost->ID;
    }
        
    /**
         * Method returns prefix
         *
         * @return string prefix
         **/
    public static function prefix()
    {
        return 'ngloadonepost';
    }
        
    /**
         * Method returns plugin version
         *
         * @return string version
         **/
    public static function version()
    {
        return '1.0';
    }
}
    
    new NgLoadonepost();
