let ngloadedpages = [];
jQuery(window).scroll(function () {

    if (ngMarkerIsVisible()) {

        ngloadnextpost();

    }
});
jQuery(document).ready(function () {
    if (ngMarkerIsVisible()) {
        ngloadnextpost();

    }
});

function ngMarkerIsVisible() {
    if (jQuery('.ngloadnext').length == 0) {
        return false;
    }
    let top_of_element = jQuery('.ngloadnext').offset().top;
    let bottom_of_element = jQuery('.ngloadnext').offset().top + jQuery('.ngloadnext').outerHeight();
    let bottom_of_screen = jQuery(window).scrollTop() + jQuery(window).innerHeight();
    let top_of_screen = jQuery(window).scrollTop();
    if (bottom_of_screen >= bottom_of_element) {
        // return true;
    }
    if ((bottom_of_screen > top_of_element) && (top_of_screen < bottom_of_element)) {
        return true;
    }
    return false;
}

function ngloadnextpost() {

    if (jQuery('.ngloadnext').hasClass('ngloading')) {
        return;
    }
    jQuery('.ngloadnext').addClass('ngloading');
    setTimeout(function () {
        jQuery('.ngloadnext').removeClass('ngloading');
    }, 5000);
    jQuery.ajax({
        url: ngloadonepost.ajax_url,
        async: true,
        data: {
            action: 'ngloadonepost',
            nextPostID: ngloadonepost.nextPostId
        },
        type: 'POST',
        success: function (data) {
            if (!ngloadonepost.nextPostId) {
                jQuery('.ngloadnext').remove();
                return;
            }
            if (ngloadedpages.includes(data.nextPostId)) {
                return;
            }
            ngloadedpages.push(data.nextPostId);
            jQuery('.ngloadnext').removeClass('ngloading');
            history.pushState('data', data.title, data.nextPostUrl);
            jQuery('.ngloadnext').before(data.content);
            ngloadonepost.nextPostId = data.nextPostId;
            if (typeof nglopcoallback !== 'undefined') {
                //callbacks for different analytics
                nglopcoallback(data.title, data.nextPostUrl);
            }
        }
    });
}
