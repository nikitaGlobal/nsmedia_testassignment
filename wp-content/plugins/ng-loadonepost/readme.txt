=== NG Load One Post ===
Contributors: nikitaglobal
Plugin Name: NG Load One Post
Tags: ajax, post, single view
Author: Nikita Menshutin
Text Domain: NgLoadonepost
Domain Path: /languages
Requires at least: 3.6
Tested up to: 5.4
Stable tag: 1.3
Requires PHP: 5.6
Version: 1.30
License: 			GPLv2 or later
License URI: 		http://www.gnu.org/licenses/gpl-2.0.html

== Description ==

Implement AJAX loading (Infinite Scroll) for the article pages.
The next article needs to be loaded when the user gets to the bottom of each article.
Make sure that you don't load more than one article at once.
The URLs need to change in the address bar when the user scrolls down to the second,
third, fourth... article.

Has js callback nglopcoallback(title, url). You can use it for analytics issues.

[https://nikita.global/](https://nikita.global)

= Filters =

ngloadonepost to filter content loaded via ajax (for theme customization)

= Actions =


== Installation ==

Use WordPress' Add New Plugin feature, searching "NG Load one post", or download the archive and:

1. Unzip the archive on your computer  
2. Upload plugin directory to the `/wp-content/plugins/` directory
3. Activate the plugin through the 'Plugins' menu in WordPress

== Changelog ==

= 1.0 (2020-05-29) =
* Finished, PSR-2 Compliant
